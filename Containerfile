ARG IMAGE_NAME=registry.fedoraproject.org/fedora
ARG IMAGE_TAG=40

FROM ${IMAGE_NAME}:${IMAGE_TAG} as builder

ARG GIT_REPO_URL=https://github.com/eclipse-ibeji/ibeji.git
ARG GIT_REPO_REFS=main
ARG BUILD_TYPE=release

RUN dnf install -y \
git \
gcc \
protobuf-devel \
protobuf-compiler \
SDL2-devel \
rust \
cargo \
openssl-devel \
cmake \
dotnet-sdk-8.0

RUN git clone --recurse-submodules -b ${GIT_REPO_REFS} ${GIT_REPO_URL} /tmp/ibeji

WORKDIR /tmp/ibeji

RUN cargo build --${BUILD_TYPE}

RUN mkdir -p /tmp/ibeji-bin && \
cp $(find target/release -maxdepth 1 -perm -111 -type f) /tmp/ibeji-bin

FROM ${IMAGE_NAME}:${IMAGE_TAG}

ENV IBEJI_HOME=/etc/ibeji

COPY files/ /

COPY --from=builder /tmp/ibeji-bin/* /usr/local/bin/
