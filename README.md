# Eclipse Ibeji

Project URL: https://github.com/eclipse-ibeji/ibeji

## Building and Pulling

Build the image by running:

```
podman run -it localhost/ibeji:latest
```

You can also pull it from quay.io instead of building it yourself:

```
podman pull quay.io/centos-sig-automotive/eclipse-ibeji:latest
```

## Running

Basic configuration files proviced in `/etc/ibeji` are to be used for local testing/running only and assumes
other all dependent services  ate available in the same network.

Running Chariott:

```sh
podman run \
-it \
--network host \
quay.io/centos-automotive-sig/eclipse-chariott:latest \
chariott
```

Running Chariott Service Discovery:

```
podman run \
-it \
--network host \
quay.io/centos-automotive-sig/eclipse-chariott:latest \
chariott-service-discovery
```

Running Ibeji:

```sh
podmam run \
-it \
--network host \
quay.io/centos-sig-automotive/eclipse-ibeji:latest \
/usr/local/bin/ibeji-invehicle-digital-twin
```
